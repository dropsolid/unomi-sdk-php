<?php

namespace Dropsolid\UnomiSdkPhp\Http\ApiClient;

use Dropsolid\UnomiSdkPhp\Request\Attributes\Offset\OffsetInterface;
use Dropsolid\UnomiSdkPhp\Request\Attributes\Size\SizeInterface;
use Dropsolid\UnomiSdkPhp\Request\Attributes\Sort\SortInterface;
use Dropsolid\UnomiSdkPhp\Request\RequestInterface;
use Http\Discovery\Psr17Factory;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Http\Client\ClientInterface;

/**
 * Class ApiClient
 *
 * @package Dropsolid\UnomiSdkPhp\Http
 */
class ApiClient implements ApiClientInterface
{
    /**
     * @var \League\OAuth2\Client\Provider\AbstractProvider
     */
    protected $provider;

    /**
     * @var \Psr\Http\Client\ClientInterface
     */
    protected $httpClient;

    /**
     * @var \League\OAuth2\Client\Token\AccessToken
     */
    private $accessToken;

    /**
     * @var string
     */
    private $defaultMethod;

    /**
     * @var string Unomi Base URL to use
     */
    private $baseUri = '';

  /**
   * ApiClient constructor.
   *
   * @param AbstractProvider|null                 $provider
   * @param ClientInterface $httpClient
   * @param AccessTokenInterface|null             $accessToken
   * @param array                                 $options
   *       An associative array of options. Supports the following values:
   *       - `default_method`: the default method to use when a requests supports
   *       multiple methods.
   */
    public function __construct(
        $provider,
        ClientInterface $httpClient,
        $accessToken,
        array $options = []
    ) {

        $this->httpClient = $httpClient;
        $this->provider = $provider;
        $this->accessToken = $accessToken;
        $this->defaultMethod = $options['default_method'] ?? 'GET';

        // Set endpoint uri
        $this->baseUri = $options['base_uri'] ?? $this->baseUri;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD)
     *   Suppress warnings for PHPMD for now. It exceeds cyclomatic threshold but not sure how to improve it.
     */
    public function handle(RequestInterface $request)
    {
        $options = [];
        $body = $request->getBody();
        $urlParams = [];

        if ($request instanceof SortInterface) {
            $sort = $request->getSort();
            if (!empty($sort)) {
                foreach ($sort as $field => $order) {
                    $body['sort'][] = [
                        'field' => $field,
                        'order' => $order,
                    ];
                    $urlParams['sort'][] = $field . ':' . $order;
                }
                $urlParams['sort'] = implode(',', $urlParams['sort']);
            }
        }

        if ($request instanceof SizeInterface) {
            $size = $request->getSize();
            if (!empty($size)) {
                $body['size'] = $size;
                $urlParams['size'] = reset($size);
            }
        }

        if ($request instanceof OffsetInterface) {
            $offset = $request->getOffset();
            if (!empty($offset)) {
                $body['offset'] = $offset;
                $urlParams['offset'] = reset($offset);
            }
        }

        // Always request application/json
        $headers = ['Content-Type' => 'application/json','Accept' => 'application/json'];

        if (!empty($body)) {
            $body = json_encode($body);
        }

        if ($this->provider) {
            // Set the body.
            if (!empty($body)) {
                $options['body'] = $body;
            }
            // Set the headers.
            $options['headers'] = $headers;

            // Set the url parameters.
            $url = $this->baseUri . $request->getEndpoint();
            if(!empty($urlParams)){
                $url .= '?' . http_build_query($urlParams);
            }

            $psrRequest = $this->provider->getAuthenticatedRequest(
                $request->getMethod() ?: $this->defaultMethod,
                $url,
                $this->accessToken,
                $options
            );
            return $this->httpClient->sendRequest($psrRequest);
        }

        // The body can't be an empty array.
        if (empty($body)) {
          $body = '';
        }

        // If no provider was given. execute the request directly.
        $psrFactory = new Psr17Factory();
        $psrRequest = $psrFactory->createRequest(
            $request->getMethod() ?: $this->defaultMethod,
            $this->baseUri . $request->getEndpoint()
        );
        $psrRequest = $psrRequest->withBody($psrFactory->createStream($body));
        foreach ($headers as $header => $value) {
          $psrRequest = $psrRequest->withHeader($header, $value);
        }
        return $this->httpClient->sendRequest($psrRequest);
    }
}
